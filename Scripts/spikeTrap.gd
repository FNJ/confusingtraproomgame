extends KinematicBody2D

const UP = Vector2(0,-1)

var velocity = Vector2()

func _physics_process(delta):
	move_and_slide(velocity, UP)
	
func move_up(velocity_given):
	velocity = velocity_given
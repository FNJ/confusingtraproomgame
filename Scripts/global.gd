extends Node

var timer = Timer.new()
var lives = 10
var time = "00 : 00"

func _process(delta):
	time = timer.time_left
	var minutes = floor(time/60)
	var seconds = int(time)%60
	time = str(minutes) + " : " + str(seconds)

# Called when the node enters the scene tree for the first time.
func _ready():
	timer.connect("timeout", self, "_on_timer_timeout")
	timer.set_one_shot(true)
	timer.set_wait_time(300)
	#timeout is what says in docs, in signals
	#self is who respond to the callback
	#_on_timer_timeout is the callback, can have any name
	add_child(timer) #to process
	timer.start() #to start
	pass # Replace with function body.
	
func _on_timer_timeout():
	get_tree().change_scene(str("res://Scenes/Game Over.tscn"))
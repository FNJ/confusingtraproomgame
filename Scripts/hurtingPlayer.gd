extends Area2D

func _on_spikeTrap_body_entered(body):
	if body.get_name() == "player1":
		global.lives -= 1
		body.get_node("spriteanimate").play("hurt")
		get_parent().get_parent().queue_free()
		if (global.lives == 0):
			get_tree().change_scene(str("res://Scenes/Game Over.tscn"))
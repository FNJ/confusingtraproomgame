extends KinematicBody2D

var speed = 200
var GRAVITY = 1000
var jump_speed = -350

const UP = Vector2(0,-1)

var velocity = Vector2()

func get_input():
	velocity.x = 0
	if is_on_floor() and Input.is_action_just_pressed('ui_up'):
		$spriteanimate/jump.play()
		velocity.y = jump_speed
	if Input.is_action_pressed('ui_right'):
		velocity.x += speed
	if Input.is_action_pressed('ui_left'):
		velocity.x -= speed
	if is_on_floor() and Input.is_action_just_pressed("ui_right"):
		$spriteanimate/running.play()
	if is_on_floor() and Input.is_action_just_pressed("ui_left"):
		$spriteanimate/running.play()

func _physics_process(delta):
	velocity.y += delta * GRAVITY
	get_input()
	velocity = move_and_slide(velocity, UP)

func _process(delta):
	if velocity.y != 0:
		$spriteanimate.play("jump")
	elif velocity.x != 0:
		$spriteanimate.play("running")
		if velocity.x > 0:
			$spriteanimate.flip_h = false
		else:
			$spriteanimate.flip_h = true
	else:
		$spriteanimate/running.stop()
		$spriteanimate/running/running2.stop()
		$spriteanimate.play("idle")

func _on_running_finished():
	if is_on_floor() :
		$spriteanimate/running/running2.play()

func _on_running2_finished():
	if is_on_floor() :
		$spriteanimate/running.play()

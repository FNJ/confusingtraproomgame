extends Area2D

var listScene = ["Map 1","Map 1","Map 1","Map 1",
				"Map 2","Map 2","Map 2",
				"Map 3","Map 3",
				"Map 4"]

func _on_Area_Trigger_body_entered(body):
    if body.get_name() == "player1":
        listScene.shuffle()
        var sceneName = listScene[0]
        get_tree().change_scene(str("res://Scenes/" + sceneName + ".tscn"))

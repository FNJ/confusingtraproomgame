extends Area2D

var GRAVITY = 1000
export (int) var slide_y_up = -100

const UP = Vector2(0,-1)

var velocity = Vector2()

func _on_Area2D_body_entered(body):
	if body.get_name() == "player1":
		$spike.visible = true
		velocity.y = slide_y_up
		$spike.move_up(velocity)